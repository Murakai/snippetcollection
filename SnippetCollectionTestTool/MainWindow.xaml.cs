﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace SnippetCollectionTestTool
{
    /// <summary>
    /// Interaktionslogik für MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();

            TbLoginWinUser.Text = TbWinUser.Text = Environment.UserDomainName + "\\" + Environment.UserName;

            BtnSqlConnect_Click(null, null);
        }

        #region GroupBoxSql
        private void BtnSqlConnect_Click(object sender, RoutedEventArgs e)
        {
            var scsb = new System.Data.SqlClient.SqlConnectionStringBuilder();
            scsb.DataSource = TbSqlServer.Text;
            if (!string.IsNullOrWhiteSpace(TbSqlDbName.Text))
                scsb.InitialCatalog = TbSqlDbName.Text;
            if (ChkBSqlWinAUth.IsChecked ?? false)
                scsb.IntegratedSecurity = true;
            else
            {
                scsb.UserID = TbSqlUserName.Text;
                scsb.Password = TbSqlPw.Password;
            }

            SQLSingleton.SQL.Init(scsb.ConnectionString);

            if (SQLSingleton.SQL.Open())
            { 
                GroupBoxSql.IsEnabled = false;
                GridSqlConnection.Visibility = Visibility.Collapsed;
            }
            else
                MessageBox.Show("could not connect");
        }
        private void BtnSqlConnectForm_Click(object sender, RoutedEventArgs e)
        {
            SQLSingleton.SQL.Init();

            if (SQLSingleton.SQL.Open())
            {
                GroupBoxSql.IsEnabled = false;
                GridSqlConnection.Visibility = Visibility.Collapsed;
            }
            else
                MessageBox.Show("could not connect");
        }
        private void ChkBSqlWinAUth_Checked(object sender, RoutedEventArgs e)
        {
            TbSqlPw.IsEnabled = TbSqlUserName.IsEnabled = ChkBSqlWinAUth.IsChecked ?? false;
        }
        #endregion
        #region GroupBoxCreateUser
        private void BtnCreateUser_Click(object sender, RoutedEventArgs e)
        {
            var user = new SnippetCollection.User(
                TbWinUser.Text, 
                TbDisplayName.Text, 
                ChkBWinAUth.IsChecked ?? false, 
                TbUserName.Text, 
                TbPassword.Password, 
                ChkBCredAuth.IsChecked ?? false
            );

            if (user.Save())
                MessageBox.Show("success!");
            else
                MessageBox.Show("error...");

        }
        private void BtnGetUsers_Click(object sender, RoutedEventArgs e)
        {
            var users = new SnippetCollection.Users();
            MessageBox.Show("Users.Count = " + users.Count);
        }
        #endregion
        #region GroupBoxLoginUser
        private void BtnLogin_Click(object sender, RoutedEventArgs e)
        {
            SnippetCollection.User user;
            if (ChkBLoginWinAUth.IsChecked ?? false)
                user = SnippetCollection.User.Login(TbLoginWinUser.Text);
            else
                user = SnippetCollection.User.Login(TbLoginUserName.Text, TbLoginPassword.Password);

            if (user != null)
            { 
                TbLoggedInAs.Text = SnippetCollection.Global.CurrentUser.DisplayName;
                BtnUpdateCurrentUser.Visibility = BtnDeleteCurrentUser.Visibility = Visibility.Visible;
            }
            else
                MessageBox.Show("nope...");
        }
        private void BtnDeleteCurrentUser_Click(object sender, RoutedEventArgs e)
        {
            if (SnippetCollection.Global.CurrentUser == null)
                return;

            if (SnippetCollection.Global.CurrentUser.Delete())
                MessageBox.Show("success!");
            else
                MessageBox.Show("nope...");
        }
        private void BtnUpdateCurrentUser_Click(object sender, RoutedEventArgs e)
        {
            if (SnippetCollection.Global.CurrentUser == null)
                return;

            SnippetCollection.Global.CurrentUser.UserName = "test2";
            SnippetCollection.Global.CurrentUser.Password = "test2";
            SnippetCollection.Global.CurrentUser.DisplayName = SnippetCollection.Global.CurrentUser.DisplayName + "2";
            SnippetCollection.Global.CurrentUser.Save();

            MessageBox.Show(new SnippetCollection.User(SnippetCollection.Global.CurrentUser.Guid).DisplayName);
        }
        #endregion
        #region GroupBoxCreateCategory
        private void BtnCreateCategory_Click(object sender, RoutedEventArgs e)
        {
            SnippetCollection.SnippetCategory category = null;

            if (CbNewCategoryParent.SelectedItem == null)
                category = new SnippetCollection.SnippetCategory(TbNewCategoryName.Text);
            else
            {
                var item = ((SnippetCollection.SnippetCategory)CbNewCategoryParent.SelectedItem);
                category = new SnippetCollection.SnippetCategory(TbNewCategoryName.Text, item);
            }

            if (category.Save())
            {
                BtnReloadParentCategories_Click(null, null);
                MessageBox.Show("success!");
            }
            else
                MessageBox.Show("nope...");
        }
        private void BtnReloadParentCategories_Click(object sender, RoutedEventArgs e)
        {
            if (CbNewCategoryParent.SelectedItem == null)
                CbNewCategoryParent.ItemsSource = new SnippetCollection.SnippetCategories();
            else
                CbNewCategoryParent.ItemsSource = new SnippetCollection.SnippetCategories((SnippetCollection.SnippetCategory)CbNewCategoryParent.SelectedItem);
            Tree.RootItems = new SnippetCollection.SnippetCategories().ToList<CustomTreeList.ICustomTreeListItem>();
        }
        private void BtnDeleteParentCategory_Click(object sender, RoutedEventArgs e)
        {
            if (CbNewCategoryParent.SelectedItem == null)
            {
                MessageBox.Show("wrong input");
                return;
            }

            var item = ((SnippetCollection.SnippetCategory)CbNewCategoryParent.SelectedItem);

            if (item.Delete())
            {
                BtnReloadParentCategories_Click(null, null);
                MessageBox.Show("success!");
            }
            else
                MessageBox.Show("nope...");
        }
        #endregion
        #region GroupBoxCreateSnippet
        private void BtnCreateSnippet_Click(object sender, RoutedEventArgs e)
        {
            if (CbCreateSnippetCategory.SelectedItem == null || string.IsNullOrWhiteSpace(TbNewSnippetName.Text))
            {
                MessageBox.Show("wrong input");
                return;
            }

            var category = (SnippetCollection.SnippetCategory)CbCreateSnippetCategory.SelectedItem;

            var snippet = new SnippetCollection.Snippet(category, TbNewSnippetName.Text, TbNewSnippetDescription.Text, TbNewSnippetContent.Text);
            
            if (snippet.Save())
            {
                MessageBox.Show("success!");
            }
            else
                MessageBox.Show("nope...");
        }
        private void BtnSnippetLoadCategory_Click(object sender, RoutedEventArgs e)
        {
            if (CbCreateSnippetCategory.SelectedItem == null)
                CbCreateSnippetCategory.ItemsSource = new SnippetCollection.SnippetCategories();
            else
                CbCreateSnippetCategory.ItemsSource = new SnippetCollection.SnippetCategories((SnippetCollection.SnippetCategory)CbCreateSnippetCategory.SelectedItem);
        }
        #endregion
        #region GroupBoxCreateSnippetComment
        private void BtnSnippetCommentLoadCategory_Click(object sender, RoutedEventArgs e)
        {
            if (CbCreateSnippetCommentCategory.SelectedItem == null)
                CbCreateSnippetCommentCategory.ItemsSource = new SnippetCollection.SnippetCategories();
            else
                CbCreateSnippetCommentCategory.ItemsSource = new SnippetCollection.SnippetCategories((SnippetCollection.SnippetCategory)CbCreateSnippetCommentCategory.SelectedItem);
        }
        private void BtnSnippetCommentLoadSnippet_Click(object sender, RoutedEventArgs e)
        {
            if (CbCreateSnippetCommentCategory.SelectedItem == null || !(CbCreateSnippetCommentCategory.SelectedItem is SnippetCollection.SnippetCategory))
                CbCreateSnippetCommentSnippet.ItemsSource = null;
            else
                CbCreateSnippetCommentSnippet.ItemsSource = new SnippetCollection.Snippets((SnippetCollection.SnippetCategory)CbCreateSnippetCommentCategory.SelectedItem);

        }
        private void BtnSnippetCommentLoadComment_Click(object sender, RoutedEventArgs e)
        {
            if (CbCreateSnippetCommentSnippet.SelectedItem == null || !(CbCreateSnippetCommentSnippet.SelectedItem is SnippetCollection.SnippetComment))
                CbCreateSnippetCommentComment.ItemsSource = null;
            else
                CbCreateSnippetCommentComment.ItemsSource = new SnippetCollection.SnippetComments((SnippetCollection.SnippetComment)CbCreateSnippetCommentSnippet.SelectedItem);
        }
        private void BtnCreateSnippetComment_Click(object sender, RoutedEventArgs e)
        {
            if (CbCreateSnippetCommentSnippet.SelectedItem == null || !(CbCreateSnippetCommentSnippet.SelectedItem is SnippetCollection.Snippet))
            {
                MessageBox.Show("wrong input");
                return;
            }

            var comment = new SnippetCollection.SnippetComment
            (
                TbNewSnippetCommentText.Text, 
                (SnippetCollection.Snippet)CbCreateSnippetCommentSnippet.SelectedItem,
                CbCreateSnippetCommentComment.SelectedItem == null || !(CbCreateSnippetCommentComment.SelectedItem is SnippetCollection.SnippetComment) ? 
                    null : (SnippetCollection.SnippetComment)CbCreateSnippetCommentComment.SelectedItem
            );

            if (comment.Save())
            {
                MessageBox.Show("success!");
            }
            else
                MessageBox.Show("nope...");
        }
        #endregion
        #region GroupBoxCreateSnippetAttachment
        private void BtnSnippetAttachmentLoadCategory_Click(object sender, RoutedEventArgs e)
        {
            if (CbCreateSnippetAttachmentCategory.SelectedItem == null)
                CbCreateSnippetAttachmentCategory.ItemsSource = new SnippetCollection.SnippetCategories();
            else
                CbCreateSnippetAttachmentCategory.ItemsSource = new SnippetCollection.SnippetCategories((SnippetCollection.SnippetCategory)CbCreateSnippetAttachmentCategory.SelectedItem);
        }
        private void BtnSnippetAttachmentLoadSnippet_Click(object sender, RoutedEventArgs e)
        {
            if (CbCreateSnippetAttachmentCategory.SelectedItem == null || !(CbCreateSnippetAttachmentCategory.SelectedItem is SnippetCollection.SnippetCategory))
                CbCreateSnippetAttachmentSnippet.ItemsSource = null;
            else
                CbCreateSnippetAttachmentSnippet.ItemsSource = new SnippetCollection.Snippets((SnippetCollection.SnippetCategory)CbCreateSnippetAttachmentCategory.SelectedItem);

        }
        private void BtnLoadFile_Click(object sender, RoutedEventArgs e)
        {
            var ofd = new Microsoft.Win32.OpenFileDialog { Multiselect = false };
            if (!(ofd.ShowDialog() ?? false))
                return;

            TbNewSnippetAttachmentFilePath.Text = ofd.FileName;
            TbNewSnippetAttachmentName.Text = System.IO.Path.GetFileName(ofd.FileName);
        }
        private void BtnCreateSnippetAttachment_Click(object sender, RoutedEventArgs e)
        {
            if (CbCreateSnippetAttachmentSnippet.SelectedItem == null || 
                !(CbCreateSnippetAttachmentSnippet.SelectedItem is SnippetCollection.Snippet) || 
                string.IsNullOrWhiteSpace(TbNewSnippetAttachmentFilePath.Text) || 
                string.IsNullOrWhiteSpace(TbNewSnippetAttachmentName.Text) || !System.IO.File.Exists(TbNewSnippetAttachmentFilePath.Text))
            {
                MessageBox.Show("wrong input");
                return;
            }

            var attachment = new SnippetCollection.SnippetAttachment(
                (SnippetCollection.Snippet)CbCreateSnippetAttachmentSnippet.SelectedItem,
                TbNewSnippetAttachmentName.Text,
                System.IO.File.ReadAllBytes(TbNewSnippetAttachmentFilePath.Text)
            );

            if (attachment.Save())
            {
                MessageBox.Show("success!");
            }
            else
                MessageBox.Show("nope...");
        }
        #endregion
    }
}
