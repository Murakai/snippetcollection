﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SQLSingleton;
using SQLSingleton.Attributes;

namespace SnippetCollection
{
    [Table("Snippets")]
    public class Snippet : DataBaseObject
    {
        private SnippetCategory _category;
        [Field("CATEGORY")]
        public SnippetCategory Category
        {
            get { return _category; }
            set
            {
                if (_category != value)
                {
                    ChangedProperties.Add("Category");
                    _category = value;
                }
            }
        }

        private User _creator;
        [Field("CREATOR")]
        public User Creator
        {
            get { return _creator; }
            set
            {
                if (_creator != value)
                {
                    ChangedProperties.Add("Creator");
                    _creator = value;
                }
            }
        }

        private DateTime _createdAt;
        [Field("CREATEDAT")]
        public DateTime CreatedAt
        {
            get => _createdAt;
            private set
            {
                if (_createdAt != value)
                {
                    ChangedProperties.Add("CreatedAt");
                    _createdAt = value;
                }
            }
        }

        private string _title;
        [Field("TITLE")]
        public string Title
        {
            get => _title;
            set
            {
                if (_title != value)
                {
                    ChangedProperties.Add("Title");
                    _title = value;
                }
            }
        }

        private string _description;
        [Field("DESCRIPTION")]
        public string Description
        {
            get => _description;
            set
            {
                if (_description != value)
                {
                    ChangedProperties.Add("Description");
                    _description = value;
                }
            }
        }

        private string _content;
        [Field("CONTENT")]
        public string Content
        {
            get => _content;
            set
            {
                if (_content != value)
                {
                    ChangedProperties.Add("Content");
                    _content = value;
                }
            }
        }

        private SnippetComments _comments;
        public SnippetComments Comments => _comments == null ? _comments = new SnippetComments(this) : _comments;

        private SnippetAttachments _attachments;
        public SnippetAttachments Attachments => _attachments == null ? _attachments = new SnippetAttachments(this) : _attachments;

        public List<string> Tags { get; set; } = new List<string>();


        public Snippet() : base() { }
        public Snippet(Guid guid) : base(guid) { }

        public Snippet(SnippetCategory category, string title, string description, string content, params string[] tags) : this(Global.CurrentUser, category, title, description, content, tags)  { }
        public Snippet(User creator, SnippetCategory category, string title, string description, string content, params string[] tags) : base(true)
        {
            Creator = creator;
            Title = title;
            Description = description ?? "";
            Content = content ?? "";
            Category = category;
            Tags = tags != null ? tags.ToList() : new List<string>();
            CreatedAt = DateTime.Now;
        }

        public override bool Delete()
        {
            foreach (var comment in Comments)
                comment.Delete();

            foreach (var attachment in Attachments)
                attachment.Delete();

            return base.Delete();
        }

        public override string ToString()
        {
            return Title;
        }
    }

    public class Snippets : DataBaseObjectCollection<Snippet>
    {
        public Snippets() : base() { }

        public Snippets(SnippetCategory snippetCategory, bool includeChildcategories = false) : base(new NameValuePair("CATEGORY", snippetCategory.Guid))
        {
            if (includeChildcategories)
                foreach (var category in snippetCategory.ChildCategories)
                    AddRange(new Snippets(category, true));
        }
        public Snippets(User creator) : base(new NameValuePair("CREATOR", creator.Guid)) { }
    }
}
