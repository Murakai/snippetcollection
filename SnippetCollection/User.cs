﻿using SQLSingleton;
using SQLSingleton.Attributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SnippetCollection
{
    [Table("Users")]
    public class User : DataBaseObject
    {
        private string _userName;
        [Field("USERNAME")]
        public string UserName
        {
            get => _userName;
            set
            {
                if (!string.IsNullOrWhiteSpace(value) && Global.SQL.Exists("Users", new NameValuePair("USERNAME", value)))
                    return;

                if (_userName != value)
                    ChangedProperties.Add("UserName");
                _userName = value;
            }
        }

        private string _password;
        [Field("PASSWORD", isEncrypted: true)]
        public string Password
        {
            internal get { return _password; }
            set
            {
                if (_password != value.Encrypt())
                    ChangedProperties.Add("Password");
                _password = value.Encrypt();
            }
        }

        private bool _allowWindowsLogon;
        [Field("ALLOWWINDOWSLOGON")]
        public bool AllowWindowsLogon
        {
            get { return _allowWindowsLogon; }
            set
            {
                if (_allowWindowsLogon != value)
                    ChangedProperties.Add("AllowWindowsLogon");
                _allowWindowsLogon = value;
            }
        }

        private bool _allowCredentialLogon;
        [Field("ALLOWCREDENTIALLOGON")]
        public bool AllowCredentialLogon
        {
            get { return _allowCredentialLogon; }
            set
            {
                if (_allowCredentialLogon != value)
                    ChangedProperties.Add("AllowCredentialLogon");
                _allowCredentialLogon = value;
            }
        }

        private string _displayName;
        [Field("DISPLAYNAME")]
        public string DisplayName
        {
            get => !string.IsNullOrWhiteSpace(_displayName) ? _displayName :
                !string.IsNullOrWhiteSpace(_userName) ? _userName :
                _windowsUser;
            set
            {
                if (!string.IsNullOrWhiteSpace(value) && Global.SQL.Exists("Users", new NameValuePair("USERNAME", value)))
                    return;

                if (_displayName != value)
                    ChangedProperties.Add("DisplayName");
                _displayName = value;
            }
        }

        private string _windowsUser;
        [Field("WINDOWSUSER")]
        public string WindowsUser
        {
            get => _windowsUser;
            set
            {
                if (!string.IsNullOrWhiteSpace(value) && Global.SQL.Exists("Users", new NameValuePair("WINDOWSUSER", value)))
                    return;

                if (_windowsUser != value)
                    ChangedProperties.Add("WindowsUser");
                _windowsUser = value;
            }
        }

        public User() : base() { }
        public User(Guid guid) : base(guid) { }

        public User(string windowsUser, string displayName = null, bool allowWindowsLogon = true, string userName = null, string password = null, bool allowCredentialLogon = true) : base(true)
        {
            List<NameValuePair> where = new List<NameValuePair> { new NameValuePair("WINDOWSUSER", windowsUser) };

            if (!string.IsNullOrWhiteSpace(userName))
                where.Add(new NameValuePair("USERNAME", userName));

            if (!string.IsNullOrWhiteSpace(displayName))
                where.Add(new NameValuePair("DISPLAYNAME", displayName));

            if (Global.SQL.Exists("Users", SQLSingleton.Global.Conjunction.Or, where.ToArray()))
            {
                Guid = Guid.Empty;
                return;
            }

            WindowsUser = windowsUser;
            DisplayName = displayName ?? userName ?? windowsUser;
            UserName = userName;
            Password = password;
            AllowWindowsLogon = allowWindowsLogon;
            AllowCredentialLogon = allowCredentialLogon;
        }

        public static User Login(string username, string password)
        {
            if (string.IsNullOrWhiteSpace(username) || string.IsNullOrWhiteSpace(password))
                return null;

            var guid = Global.SQL.SelectScalar("[Users]", "[GUID]",
                new NameValuePair("USERNAME", username), new NameValuePair("PASSWORD", password.Encrypt()), new NameValuePair("ALLOWCREDENTIALLOGON", true));
            if (guid == null)
                return null;

            return Global.CurrentUser = new User((Guid)guid);
        }

        public bool Logout()
        {
            if (null == Global.CurrentUser || this != Global.CurrentUser)
                return false;

            Global.CurrentUser = null;

            return true;
        }

        public static User Login(string windowsUser)
        {
            var guid = Global.SQL.SelectScalar("[Users]", "[GUID]",
                new NameValuePair("WINDOWSUSER", windowsUser), new NameValuePair("ALLOWWINDOWSLOGON", true));
            if (guid == null)
                return null;

            return Global.CurrentUser = new User((Guid)guid);
        }

        public override bool Delete()
        {
            if (this == Global.CurrentUser)
                Global.CurrentUser = null;

            return base.Delete();
        }
    }

    public class Users : DataBaseObjectCollection<User>
    {
        public Users() : base()
        {

        }
    }
}
