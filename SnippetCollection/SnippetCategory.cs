﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CustomTreeList;
using SQLSingleton;
using SQLSingleton.Attributes;

namespace SnippetCollection
{
    [Table("SnippetCategories")]
    public class SnippetCategory : DataBaseObject, ICustomTreeListItem
    {
        private User _creator;
        [Field("CREATOR")]
        public User Creator
        {
            get { return _creator; }
            set
            {
                if (_creator != value)
                {
                    ChangedProperties.Add("Creator");
                    _creator = value;
                }
            }
        }

        private SnippetCategory _parent;
        [Field("PARENT")]
        public SnippetCategory Parent
        {
            get { return _parent; }
            set
            {
                if (_parent != value)
                {
                    ChangedProperties.Add("Parent");
                    _parent = value;
                }
            }
        }

        private string _name;
        [Field("NAME")]
        public string Name
        {
            get => _name;
            set
            {
                if (_name != value)
                {
                    ChangedProperties.Add("Name");
                    _name = value;
                }
            }
        }

        private Snippets _snippets;
        public Snippets Snippets => _snippets == null ? _snippets = new Snippets(this) : _snippets;

        public SnippetCategories ChildCategories =>  new SnippetCategories(this);

        public string Path => Parent == null ? Name : Parent.Path + "/" + Name;

        public ICustomTreeListItem TreeListParent => Parent;
        public IEnumerable<ICustomTreeListItem> TreeListChildren => ChildCategories;
        public int SoringIndex { get; set; }
        public string TreeListSufix { get; set; }
        public string TreeListPrefix { get; set; }

        private string _treeListText;
        public string TreeListText
        {
            get
            {
                if(string.IsNullOrWhiteSpace(_treeListText))
                    return Name;

                return _treeListText;
            }
            set { _treeListText = value; }
        }


        public SnippetCategory() : base() { }
        public SnippetCategory(Guid guid) : base(guid) { }
        public SnippetCategory(string name, SnippetCategory parent = null) : this(Global.CurrentUser, name, parent) { }
        public SnippetCategory(User creator, string name, SnippetCategory parent = null) : base(true)
        {
            Creator = creator;
            Name = name;
            Parent = parent;
        }
        /*
        public void LoadChildCategories(bool recursivly = true)
        {
            _childCategories = new SnippetCategories(this, recursivly);
        }
        */
        public override bool Delete()
        {
            foreach (var category in ChildCategories)
                category.Parent = Parent;

            foreach (var snippet in Snippets)
                snippet.Delete();

            return base.Delete();
        }

        public List<SnippetCategory> AllChildren(bool includeSelf = true)
        {
            List<SnippetCategory> result = new List<SnippetCategory>();
            if(includeSelf)
                result.Add(this);

            result.AddRange(ChildCategories);

            foreach (var category in ChildCategories)
                result.AddRange(category.AllChildren(false));

            return result;
        }

        public override string ToString()
        {
            return Name;
        }
    }

    public class SnippetCategories : DataBaseObjectCollection<SnippetCategory>
    {
        public SnippetCategories() : base(new NameValuePair("PARENT", Guid.Empty)) { }
        public SnippetCategories(SnippetCategory parentCategory) : base(new NameValuePair("PARENT", parentCategory.Guid)) { }
        public SnippetCategories(User creator) : base(new NameValuePair("CREATOR", creator.Guid)) { }
    }
}
