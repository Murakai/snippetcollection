﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SQLSingleton;
using SQLSingleton.Attributes;

namespace SnippetCollection
{
    [Table("SnippetComments")]
    public class SnippetComment : DataBaseObject
    {
        private Snippet _snippet;
        [Field("SNIPPET")]
        public Snippet Snippet
        {
            get { return _snippet; }
            set
            {
                if (_snippet != value)
                {
                    ChangedProperties.Add("Snippet");
                    _snippet = value;
                }
            }
        }

        private User _creator;
        [Field("CREATOR")]
        public User Creator
        {
            get { return _creator; }
            set
            {
                if (_creator != value)
                {
                    ChangedProperties.Add("Creator");
                    _creator = value;
                }
            }
        }

        private DateTime _createdAt;
        [Field("CREATEDAT")]
        public DateTime CreatedAt
        {
            get => _createdAt;
            private set
            {
                if (_createdAt != value)
                {
                    ChangedProperties.Add("CreatedAt");
                    _createdAt = value;
                }
            }
        }

        private SnippetComment _parent;
        [Field("PARENT")]
        public SnippetComment Parent
        {
            get => _parent;
            private set
            {
                if (_parent != value)
                {
                    ChangedProperties.Add("Parent");
                    _parent = value;
                }
            }
        }
        
        private string _text;
        [Field("TEXT")]
        public string Text
        {
            get => _text;
            set
            {
                if (_text != value)
                {
                    ChangedProperties.Add("Text");
                    _text = value;
                }
            }
        }

        private SnippetComments _childComments;
        public SnippetComments ChildComments => _childComments == null ? _childComments = new SnippetComments(this) : _childComments;

        public SnippetComment() : base() { }
        public SnippetComment(Guid guid) : base(guid) { }

        public SnippetComment(string text, Snippet snippet, SnippetComment parent = null) : this(Global.CurrentUser, text, snippet, parent) { }
        public SnippetComment(User creator, string text, Snippet snippet, SnippetComment parent = null) : base(true)
        {
            Creator = creator;
            Snippet = snippet;
            Text = text;
            Parent = parent;
            CreatedAt = DateTime.Now;
        }

        public void LoadChildCommentsRecursivly()
        {
            foreach (var comment in ChildComments)
                comment.LoadChildCommentsRecursivly();
        }

        public override bool Delete()
        {
            foreach (var comment in ChildComments)
                comment.Delete();

            return base.Delete();
        }

        public override string ToString()
        {
            return CreatedAt.ToString(Global.DateTimeFormat) + " " + Creator.DisplayName + ": " + Text;
        }
    }

    public class SnippetComments : DataBaseObjectCollection<SnippetComment>
    {
        public SnippetComments() : base() { }

        public SnippetComments(Snippet snippet, bool includeChildComments = false) : base(new NameValuePair("SNIPPET", snippet.Guid))
        {
            if (includeChildComments)
                foreach (var comment in this)
                    comment.LoadChildCommentsRecursivly();
        }

        public SnippetComments(SnippetComment comment, bool includeChildComments = false) : base(new NameValuePair("PARENT", comment.Guid))
        {
            if (includeChildComments)
                foreach (var child in this)
                    child.LoadChildCommentsRecursivly();
        }
        public SnippetComments(User creator) : base(new NameValuePair("CREATOR", creator.Guid)) { }
    }
}
