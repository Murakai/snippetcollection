﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;
using SQLSingleton;
using SQLSingleton.Attributes;

namespace SnippetCollection
{
    [Table("SnippetAttachments")]
    public class SnippetAttachment : DataBaseObject
    {
        private Snippet _snippet;
        [Field("SNIPPET")]
        public Snippet Snippet
        {
            get { return _snippet; }
            set
            {
                if (_snippet != value)
                {
                    ChangedProperties.Add("Snippet");
                    _snippet = value;
                }
            }
        }

        private User _creator;
        [Field("CREATOR")]
        public User Creator
        {
            get { return _creator; }
            set
            {
                if (_creator != value)
                {
                    ChangedProperties.Add("Creator");
                    _creator = value;
                }
            }
        }

        private DateTime _createdAt;
        [Field("CREATEDAT")]
        public DateTime CreatedAt
        {
            get => _createdAt;
            private set
            {
                if (_createdAt != value)
                {
                    ChangedProperties.Add("CreatedAt");
                    _createdAt = value;
                }
            }
        }

        private string _name;
        [Field("NAME")]
        public string Name
        {
            get => _name;
            set
            {
                if (_name != value)
                {
                    ChangedProperties.Add("Name");
                    _name = value;
                }
            }
        }

        private byte[] _hash;
        [Field("HASH")]
        public byte[] Hash
        {
            get => _hash;
            set
            {
                if (_hash != value)
                {
                    ChangedProperties.Add("Hash");
                    _hash = value;
                }
            }
        }

        private long _size;
        [Field("SIZE")]
        public long Size
        {
            get => _size;
            private set
            {
                if (_size != value)
                {
                    ChangedProperties.Add("Size");
                    _size = value;
                }
            }
        }

        private byte[] _data;

        [Field("DATA", true)]
        public byte[] Data
        {
            get
            {
                if (_data == null)
                    _data = (byte[])Global.SQL.SelectScalar("SNIPPETATTACHMENTS", "[DATA]", new NameValuePair("[GUID]", Guid));

                return _data;
            }
            set
            {
                var md5 = MD5.Create();
                var hash = md5.ComputeHash(value);
                if (_hash == null || _hash != hash)
                {
                    Hash = hash;
                    Size = value.Length;
                    ChangedProperties.Add("Data");
                    _data = value;
                }

            }
        }

        public System.Windows.Media.Imaging.BitmapImage Icon => Global.GetIconByExtension("." + Name.Split('.').Last());

        public SnippetAttachment() : base() { }
        public SnippetAttachment(Guid guid) : base(guid) { }

        public SnippetAttachment(Snippet snippet, string name, byte[] data) : this(Global.CurrentUser, snippet, name, data) { }
        public SnippetAttachment(User creator, Snippet snippet, string name, byte[] data) : base(true)
        {
            Creator = creator;
            Name = name;
            Data = data;
            Snippet = snippet;
            Size = data.Length;
            CreatedAt = DateTime.Now;
        }

        public override string ToString()
        {
            return Name;
        }
    }

    public class SnippetAttachments : DataBaseObjectCollection<SnippetAttachment>
    {
        public SnippetAttachments() : base() { }

        public SnippetAttachments(Snippet snippet) : base(new NameValuePair("SNIPPET", snippet.Guid)) { }
        public SnippetAttachments(User creator) : base(new NameValuePair("CREATOR", creator.Guid)) { }
        
    }
}
