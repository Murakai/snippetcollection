﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SnippetCollection
{
    public class Global
    {
        public static string DateTimeFormat = "dd.MM., HH:mm";

        public static User CurrentUser { get; internal set; }

        private static bool _isInitialized = false;
        public static bool IsInitialized => _isInitialized || Init();

        public static SQLSingleton.SQL SQL => SQLSingleton.SQL.Instance;

        public static bool Init()
        {
            try
            {
                return _isInitialized = SQLSingleton.SQL.Init();
            }
            catch
            {
                return _isInitialized = false;
            }
        }

        #region extract icon code
        private static Microsoft.Win32.RegistryKey RootKey = Microsoft.Win32.RegistryKey.OpenBaseKey(Microsoft.Win32.RegistryHive.ClassesRoot, Microsoft.Win32.RegistryView.Default);

        [System.Runtime.InteropServices.DllImport("shell32.dll", EntryPoint = "ExtractIconA", CharSet = System.Runtime.InteropServices.CharSet.Ansi, SetLastError = true, ExactSpelling = true)]
        private static extern IntPtr ExtractIcon(int hInst, string lpszExeFileName, int nIconIndex);

        public static System.Windows.Media.Imaging.BitmapImage GetIconByExtension(string extension)
        {
            if (string.IsNullOrWhiteSpace(extension))
                return null;

            var extensionKey = RootKey.OpenSubKey(extension.StartsWith(".") ? extension : "." + extension, false);

            if (extensionKey == null)
                return null;

            var defaultIconValue = (string)extensionKey.GetValue("", "");
            extensionKey.Close();

            if (string.IsNullOrWhiteSpace(defaultIconValue))
                return null;

            string programmValue;
            int iconIndex = 0;

            if (defaultIconValue.Contains(","))
            {
                var parts = defaultIconValue.Split(',');
                programmValue = parts.First();
                int.TryParse(parts.Last(), out iconIndex);
            }
            else
                programmValue = defaultIconValue;

            var programKey = RootKey.OpenSubKey(programmValue + "\\DefaultIcon", false);

            if (programKey == null)
                return null;

            var programmPath = (string)programKey.GetValue("", "");
            programKey.Close();

            if (string.IsNullOrWhiteSpace(programmPath))
                return null;

            programmPath = programmPath.Replace("\"", "");

            IntPtr iconIntPtr = ExtractIcon(0, programmPath, iconIndex);

            var icon = System.Drawing.Icon.FromHandle(iconIntPtr);

            var bitmap = new System.Windows.Media.Imaging.BitmapImage();
            bitmap.BeginInit();
            icon.Save(bitmap.StreamSource);
            bitmap.CacheOption = System.Windows.Media.Imaging.BitmapCacheOption.OnLoad;
            bitmap.EndInit();
            bitmap.Freeze();

            return bitmap;
        }
        #endregion
        #region readableSizeString
        //source: https://stackoverflow.com/revisions/14488941/12
        static readonly string[] SizeSuffixes =
            { "B", "KB", "MB", "GB", "TB", "PB", "EB", "ZB", "YB" };
        public static string SizeSuffix(long value, int decimalPlaces = 1)
        {
            if (decimalPlaces < 0) { throw new ArgumentOutOfRangeException("decimalPlaces"); }
            if (value < 0) { return "-" + SizeSuffix(-value); }
            if (value == 0) { return string.Format("{0:n" + decimalPlaces + "} bytes", 0); }

            // mag is 0 for bytes, 1 for KB, 2, for MB, etc.
            int mag = (int)Math.Log(value, 1024);

            // 1L << (mag * 10) == 2 ^ (10 * mag) 
            // [i.e. the number of bytes in the unit corresponding to mag]
            decimal adjustedSize = (decimal)value / (1L << (mag * 10));

            // make adjustment when the value is large enough that
            // it would round up to 1000 or more
            if (Math.Round(adjustedSize, decimalPlaces) >= 1000)
            {
                mag += 1;
                adjustedSize /= 1024;
            }

            return string.Format("{0:n" + decimalPlaces + "} {1}",
                adjustedSize,
                SizeSuffixes[mag]);
        }
        #endregion
    }
}
