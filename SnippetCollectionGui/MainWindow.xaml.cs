﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace SnippetCollectionGui
{
    /// <summary>
    /// Interaktionslogik für MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        private string _windowtitle;

        public MainWindow()
        {
            InitializeComponent();
            _windowtitle = this.Title;

            //CategoriesControl.Load();
            CategoriesControl.SelectedCategoryChanged += CategoriesControlOnSelectedCategoryChanged;
        }

        private void CategoriesControlOnSelectedCategoryChanged(object sender, SelectedCategoryChangedEventHandlerArgs args)
        {
            if (args.SelectedCategory == null)
            {
                Title = _windowtitle;
                //TODO empty snippets
                return;
            }

            this.Title = _windowtitle + " - " + args.SelectedCategory.Path;
            SnippetControl.SetCategory(args.SelectedCategory);
        }

        private void BtnAddSnippet_Click(object sender, RoutedEventArgs e)
        {
        }
    }
}
