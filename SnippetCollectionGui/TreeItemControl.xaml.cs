﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using SnippetCollection;

namespace SnippetCollectionGui
{
    /// <summary>
    /// Interaction logic for TreeItemControl.xaml
    /// </summary>
    public partial class TreeItemControl : UserControl
    {
        public TreeItemControl ParentItem { get; set; }
        public SnippetCategory Category { get; set; }
        public List<TreeItemControl> Items { get; private set; } = new List<TreeItemControl>();

        private bool _selected;
        public bool Selected
        {
            get => _selected;
            set
            {
                // ReSharper disable once AssignmentInConditionalExpression
                Background = (_selected = value) ? Brushes.DarkGray : Brushes.White;
            }
        }

        private bool _opened;
        public bool Opened
        {
            get => _opened;
            set
            {
                // ReSharper disable once AssignmentInConditionalExpression
                ChildrenStackPanel.Visibility = (_opened = value) ? Visibility.Visible : Visibility.Collapsed;
                IconImage.Source = value ? (Items.Any() ? TreeControl.FolderOpenedImage : TreeControl.FolderEmptyImage) : TreeControl.FolderClosedImage;
            }
        }

        public TreeItemControl()
        {
            InitializeComponent();
        }

        public TreeItemControl(SnippetCategory category, TreeItemControl parent = null)
        {
            InitializeComponent();

            ParentItem = parent;
            Category = category;

            TbName.Text = category.Name;
        }

        public List<TreeItemControl> Refresh()
        {
            ChildrenStackPanel.Children.Clear();

            Items = Category.ChildCategories.Select(x =>
            {
                var item = new TreeItemControl(x, this);
                ChildrenStackPanel.Children.Add(item);
                return item;
            }).ToList();

            List<TreeItemControl> children = new List<TreeItemControl>();

            foreach (var item in Items)
                children.AddRange(item.Refresh());

            return Items.Concat(children).ToList();
        }

        private void TreeItemControl_OnMouseEnter(object sender, MouseEventArgs e)
        {
            if (!Selected)
                Background = Brushes.LightGray;
        }
        private void TreeItemControl_OnMouseLeave(object sender, MouseEventArgs e)
        {
            if (!Selected)
                Background = Brushes.White;
        }

        internal event TreeItemClickedEventHandler OnItemClicked = delegate { };
        internal delegate void TreeItemClickedEventHandler(object sender, TreeItemClickedEventArgs args);
        private void TreeItemControl_OnPreviewMouseDown(object sender, MouseButtonEventArgs e)
        {
            OnItemClicked(this, new TreeItemClickedEventArgs(this));
        }

        private void IconImage_OnMouseDown(object sender, MouseButtonEventArgs e)
        {
            Opened = !Opened;
        }
    }

    internal class TreeItemClickedEventArgs
    {
        public TreeItemControl TreeItemControl;

        public TreeItemClickedEventArgs(TreeItemControl treeItemControl)
        {
            TreeItemControl = treeItemControl;
        }
    }
}
