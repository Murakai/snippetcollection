﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using SnippetCollection;

namespace SnippetCollectionGui
{
    /// <summary>
    /// Interaction logic for SnippetControl.xaml
    /// </summary>
    public partial class SnippetControl : UserControl
    {
        public SnippetControl()
        {
            InitializeComponent();
        }

        public void SetCategory(SnippetCategory category, bool includeChildCategories = false)
        {
            SnippetList.SetCategory(category, includeChildCategories);
        }
    }
}
