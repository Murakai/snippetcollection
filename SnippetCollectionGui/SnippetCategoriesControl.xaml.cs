﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using SnippetCollection;

namespace SnippetCollectionGui
{
    /// <summary>
    /// Interaktionslogik für SnippetCategoriesControl.xaml
    /// </summary>
    public partial class SnippetCategoriesControl : UserControl
    {
        public SnippetCategory Root { get; }

        //private TreeViewItem _rootItem;
        private List<SnippetCategory> _categories;

        public SnippetCategory SelectedCategory => CategoryTree.SelectedItem?.Category;

        private bool _showHeader;

        public bool ShowHeader
        {
            get => _showHeader;
            set => TbCategoriesTitle.Visibility = DpButtons.Visibility =
                (_showHeader = value) ? Visibility.Visible : Visibility.Collapsed;
        }

        public SnippetCategoriesControl()
        {
            InitializeComponent();
#if DEBUG
            Load();
            CategoryTree.OpenAll();
#endif
        }

        public SnippetCategoriesControl(SnippetCategory root, bool showHeader = true)
        {
            InitializeComponent();

            Root = root;

            ShowHeader = showHeader;
        }

        public void Load()
        {
            CategoryTree.Load(Root);
            _categories = CategoryTree.Items.Select(x => x.Category).ToList();
        }

        private void BtnAddCategory_OnClick(object sender, RoutedEventArgs e)
        {
            var addWindow = new AddSnippetCategoryWindow(SelectedCategory);
            
            if(addWindow.ShowDialog()??false)
                Load();
        }

        private void BtnDeleteCategory_OnClick(object sender, RoutedEventArgs e)
        {
            if(CategoryTree.SelectedItem == null)
                return;

            var category = SelectedCategory;

            if (MessageBox.Show("Do you realy want to delete '" + category.Name + "'?\r\n\r\n" +
                                "This includes:\r\n" +
                                "  - all child categories\r\n" +
                                "  - all snippets within each category\r\n" +
                                "  - their comments and attachments", "Are you sure?", MessageBoxButton.YesNo, MessageBoxImage.Warning) != MessageBoxResult.Yes)
                return;

            category.Delete();
            Load();

            MessageBox.Show("Category deleted.", "Done", MessageBoxButton.OK, MessageBoxImage.Information);
        }

        public event SelectedCategoryChangedEventHandler SelectedCategoryChanged = delegate { };
        public delegate void SelectedCategoryChangedEventHandler(object sender, SelectedCategoryChangedEventHandlerArgs args);
        private void CategoryTree_OnOnSelectedItemChanged(object sender, SelectedItemChangedEventArgs args)
        {
            BtnDeleteCategory.IsEnabled = CategoryTree.SelectedItem != null;
            SelectedCategoryChanged(this, new SelectedCategoryChangedEventHandlerArgs(SelectedCategory));
        }
    }

    public class SelectedCategoryChangedEventHandlerArgs
    {
        public SnippetCategory SelectedCategory { get; }

        public SelectedCategoryChangedEventHandlerArgs(SnippetCategory category)
        {
            SelectedCategory = category;
        }
    }
}
