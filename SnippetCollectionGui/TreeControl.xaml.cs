﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using SnippetCollection;

namespace SnippetCollectionGui
{
    /// <summary>
    /// Interaction logic for TreeControl.xaml
    /// </summary>
    public partial class TreeControl : UserControl
    {
        public TreeItemControl Category { get; private set; }
        public ObservableCollection<TreeItemControl> Items { get; private set; } = new ObservableCollection<TreeItemControl>();
        public TreeItemControl SelectedItem { get; private set; }

        private static BitmapImage _folderClosedBitmapImage;
        public static BitmapImage FolderClosedImage
        {
            get
            {
                if (_folderClosedBitmapImage == null)
                {
                    _folderClosedBitmapImage = new BitmapImage();
                    _folderClosedBitmapImage.BeginInit();
                    _folderClosedBitmapImage.UriSource = new Uri("pack://application:,,/Resources/FolderClosed.png");
                    _folderClosedBitmapImage.EndInit();
                }

                return _folderClosedBitmapImage;
            }
            set => _folderClosedBitmapImage = value;
        }

        private static BitmapImage _folderOpenedBitmapImage;
        public static BitmapImage FolderOpenedImage
        {
            get
            {
                if (_folderOpenedBitmapImage == null)
                {
                    _folderOpenedBitmapImage = new BitmapImage();
                    _folderOpenedBitmapImage.BeginInit();
                    _folderOpenedBitmapImage.UriSource = new Uri("pack://application:,,/Resources/FolderOpened.png");
                    _folderOpenedBitmapImage.EndInit();
                }

                return _folderOpenedBitmapImage;
            }
            set => _folderOpenedBitmapImage = value;
        }

        private static BitmapImage _folderEmptyBitmapImage;
        public static BitmapImage FolderEmptyImage
        {
            get
            {
                if (_folderEmptyBitmapImage == null)
                {
                    _folderEmptyBitmapImage = new BitmapImage();
                    _folderEmptyBitmapImage.BeginInit();
                    _folderEmptyBitmapImage.UriSource = new Uri("pack://application:,,/Resources/FolderEmpty.png");
                    _folderEmptyBitmapImage.EndInit();
                }

                return _folderEmptyBitmapImage;
            }
            set => _folderEmptyBitmapImage = value;
        }

        private static BitmapImage _documentImage;
        public static BitmapImage DocumentImage
        {
            get
            {
                if (_documentImage == null)
                {
                    _documentImage = new BitmapImage();
                    _documentImage.BeginInit();
                    _documentImage.UriSource = new Uri("pack://application:,,,/AssemblyName;component/Resources/Document.png");
                    _documentImage.EndInit();
                }

                return _documentImage;
            }
            set => _documentImage = value;
        }

        public SnippetCategory Root { get; private set; }
        private TreeItemControl _rootItem;
        public TreeItemControl RootItem {
            get
            {
                if(_rootItem == null && Root != null)
                    _rootItem = new TreeItemControl(Root);

                return _rootItem;
            }
            private set => _rootItem = value;
        }

        public TreeControl()
        {
            InitializeComponent();
        }

        public TreeControl(SnippetCategory root)
        {
            InitializeComponent();

            Root = root;
        }

        public void Load(SnippetCategory selectedCategory = null)
        {
            var openedDictionary = new List<Guid>();

            if (Items != null)
            {
                foreach (var treeItemControl in Items)
                {
                    openedDictionary.Add(treeItemControl.Category.Guid);
                }
            }

            Items = new ObservableCollection<TreeItemControl>();
            ItemsStackPanel.Children.Clear();
            if (RootItem == null)
            {
                foreach (var category in new SnippetCategories())
                {
                    var item = new TreeItemControl(category);
                    item.OnItemClicked += TreeItemControl_OnOnItemClicked;
                    item.Opened = openedDictionary.Contains(category.Guid);
                    Items.Add(item);
                    ItemsStackPanel.Children.Add(item);
                    item.Refresh().ForEach(x =>
                    {
                        x.OnItemClicked += TreeItemControl_OnOnItemClicked;
                        x.Opened = openedDictionary.Contains(x.Category.Guid);
                        Items.Add(x);
                    });
                }
            }
            else
            {
                Items.Add(RootItem);
                RootItem.OnItemClicked += TreeItemControl_OnOnItemClicked;
                RootItem.Refresh().ForEach(x =>
                {
                    x.OnItemClicked += TreeItemControl_OnOnItemClicked;
                    x.Opened = openedDictionary.Contains(x.Category.Guid);
                    Items.Add(x);
                });
            }

            if (selectedCategory != null && Items.Count(x => x.Category.Guid == selectedCategory.Guid) > 0)
                TreeItemControl_OnOnItemClicked(this, new TreeItemClickedEventArgs(Items.First(x => x.Category.Guid == selectedCategory.Guid)));
        }

        public void OpenAll()
        {
            foreach (var treeItemControl in Items)
                treeItemControl.Opened = true;
        }

        private void TreeItemControl_OnOnItemClicked(object sender, TreeItemClickedEventArgs args)
        {
            if(SelectedItem == args.TreeItemControl)
                return;

            if (SelectedItem != null)
                SelectedItem.Selected = false;

            args.TreeItemControl.Selected = true;
            SelectedItem = args.TreeItemControl;

            OnSelectedItemChanged(this, new SelectedItemChangedEventArgs(SelectedItem));
        }

        internal event SelectedItemChangedEventHandler OnSelectedItemChanged = delegate { };
        internal delegate void SelectedItemChangedEventHandler(object sender, SelectedItemChangedEventArgs args);
    }

    internal class SelectedItemChangedEventArgs
    {
        public TreeItemControl SelectedItem { get; set; }

        public SelectedItemChangedEventArgs(TreeItemControl selectedItem)
        {
            SelectedItem = selectedItem;
        }
    }
}
