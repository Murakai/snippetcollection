﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using SnippetCollection;

namespace SnippetCollectionGui
{
    /// <summary>
    /// Interaction logic for SnippetListControl.xaml
    /// </summary>
    public partial class SnippetListControl : UserControl
    {
        public SnippetCategory Category { get; private set; }
        public ObservableCollection<SnippetListItemControl> Items { get; private set; } = new ObservableCollection<SnippetListItemControl>();
        public SnippetListItemControl SelectedItem { get; private set; }
        
        public SnippetListControl()
        {
            InitializeComponent();

#if DEBUG
            for (var i = 0; i < 35; i++)
            {
                var control = new SnippetListItemControl();
                control.OnItemClicked += SnippetListItemControl_OnOnItemClicked;
                SpSnippets.Children.Add(control);
            }
#endif
        }

        public SnippetListControl(SnippetCategory category, bool includeChildCategories = false)
        {
            InitializeComponent();

            Category = category;
            Load(includeChildCategories);
        }

        public void Load(bool includeChildCategories = false)
        {
            if(Category == null)
                return;

            Items = new ObservableCollection<SnippetListItemControl>();
            SelectedItem = null;
            SpSnippets.Children.Clear();
            
            foreach (var snippet in new Snippets(Category, !includeChildCategories))
            {
                var control = new SnippetListItemControl(snippet);
                Items.Add(control);
                SpSnippets.Children.Add(control);
            }
        }

        public void SetCategory(SnippetCategory category, bool includeChildCategories = false)
        {
            Category = category;
            Load(includeChildCategories);
        }

        private void BtnAddSnippet_OnClick(object sender, RoutedEventArgs e)
        {
            MessageBox.Show("NYI");
        }

        private void SnippetListItemControl_OnOnItemClicked(object sender, ItemClickedEventArgs args)
        {
            if(SelectedItem != null)
                SelectedItem.Selected = false;

            args.Item.Selected = true;
            SelectedItem = args.Item;
        }
    }
}
