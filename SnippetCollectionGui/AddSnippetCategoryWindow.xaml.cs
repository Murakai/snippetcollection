﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using SnippetCollection;

namespace SnippetCollectionGui
{
    /// <summary>
    /// Interaktionslogik für AddSnippetCategoryWindow.xaml
    /// </summary>
    public partial class AddSnippetCategoryWindow : Window
    {
        public AddSnippetCategoryWindow()
        {
            InitializeComponent();

            Load();
            CbCategory.SelectedIndex = 0;
        }

        public AddSnippetCategoryWindow(SnippetCategory parent)
        {
            InitializeComponent();

            Load();

            if (parent == null)
                CbCategory.SelectedIndex = 0;
            else
                for(var index = 1; index < CbCategory.Items.Count; index++)
                    if(((CategoryItem)CbCategory.Items[index]).Category.Guid == parent.Guid)
                        CbCategory.SelectedIndex = index;
        }

        private void Load()
        {
            CbCategory.ItemsSource = new SnippetCategories().Select(x => new CategoryItem(x));
        }

        private void BtnAbort_OnClick(object sender, RoutedEventArgs e)
        {
            DialogResult = false;
        }

        private void BtnAdd_OnClick(object sender, RoutedEventArgs e)
        {
            #region input check
            if (CbCategory.SelectedIndex == -1)
            {
                MessageBox.Show("No category provided!", "ERROR", MessageBoxButton.OK, MessageBoxImage.Error);
                return;
            }

            if (string.IsNullOrWhiteSpace(TbName.Text))
            {
                MessageBox.Show("No name provided!", "ERROR", MessageBoxButton.OK, MessageBoxImage.Error);
                return;
            }
            #endregion

            var category = new SnippetCategory(TbName.Text, ((CategoryItem)CbCategory.SelectedItem).Category);
            category.Save();
            
            DialogResult = true;
        }
    }

    internal class CategoryItem
    {
        public SnippetCategory Category { get; }
        public string Path { get; }

        public CategoryItem(SnippetCategory category)
        {
            Category = category;
            Path = category == null ? "ROOT" : category.Path;
        }

        public override string ToString()
        {
            return Path;
        }
    }
}
