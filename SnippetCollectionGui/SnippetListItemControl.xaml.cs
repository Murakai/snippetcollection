﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using SnippetCollection;

namespace SnippetCollectionGui
{
    /// <summary>
    /// Interaction logic for SnippetListItemControl.xaml
    /// </summary>
    public partial class SnippetListItemControl : UserControl
    {
        public Snippet Snippet { get; set; }

        private bool _selected;
        public bool Selected
        {
            get => _selected;
            // ReSharper disable once AssignmentInConditionalExpression
            set => Background = (_selected = value) ? Brushes.DarkGray : Brushes.Transparent;
        }

        public SnippetListItemControl()
        {
            InitializeComponent();
        }

        public SnippetListItemControl(Snippet snippet)
        {
            Snippet = snippet;
        }

        internal event ItemClickedEventHandler OnItemClicked = delegate { };
        internal delegate void ItemClickedEventHandler(object sender, ItemClickedEventArgs args);
        private void ListItem_Clicked(object sender, MouseButtonEventArgs e)
        {
            OnItemClicked(this, new ItemClickedEventArgs(this));
        }

        private void Grid_MouseEnter(object sender, MouseEventArgs e)
        {
            if(!Selected)
                Background = Brushes.LightGray;
        }

        private void Grid_MouseLeave(object sender, MouseEventArgs e)
        {
            if(!Selected)
                Background = Brushes.Transparent;
        }
    }

    internal class ItemClickedEventArgs
    {
        public SnippetListItemControl Item { get; private set; }

        public ItemClickedEventArgs(SnippetListItemControl item)
        {
            Item = item;
        }
    }
}
